package com.example.services;

import com.example.entities.Consultation;
import com.example.entities.Medecin;
import com.example.entities.Patient;
import com.example.entities.RendezVous;

public interface ServicePack {

	Patient savePatient(Patient patient);

	Medecin saveMedecin(Medecin medecin);

	RendezVous saveRendezVous(RendezVous rendezVous);

	Consultation saveConsultation(Consultation consultation);

}
