package com.example;

import java.util.Date;
import java.util.stream.Stream;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.example.entities.Consultation;
import com.example.entities.Medecin;
import com.example.entities.Patient;
import com.example.entities.RendezVous;
import com.example.entities.StatusRDV;
import com.example.repositories.MedecinRepository;
import com.example.repositories.PatientRepository;
import com.example.repositories.RendezVousRepository;
import com.example.services.ServicePack;
import com.example.services.ServicesImpl;

@SpringBootApplication
public class SpringDataManyToOneOneToManyApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringDataManyToOneOneToManyApplication.class, args);
	}

	/*
	 * (PatientRepository patientRepository, MedecinRepository medecinRepository,
	 * RendezVousRepository rendezVousRepository, ConsultationRepository
	 * consultationRepository)
	 */
	@Bean
	CommandLineRunner start(ServicesImpl servicesImpl, ServicePack servicePack, PatientRepository patientRepository,
			MedecinRepository medecinRepository, RendezVousRepository rendezVousRepository) {
		return args -> {
			Stream.of("Mohamed", "Salah", "Sarra").forEach(t -> {
				Patient patient = new Patient();
				patient.setNom(t);
				patient.setDateNaissance(new Date());
				patient.setMalade(true);
				// patientRepository.save(patient);
				servicesImpl.savePatient(patient);
			});
			Stream.of("Said", "Taher", "Amine").forEach(name -> {
				Medecin medecin = new Medecin();
				medecin.setName(name);
				medecin.setEmail(name + "@gmail.com");
				medecin.setSpecialite(Math.random() > 0.5 ? "Cardio" : "Dentiste");
				// medecinRepository.save(medecin);
				servicesImpl.saveMedecin(medecin);
			});
			Patient patient = patientRepository.findById(1L).orElse(null);

			Medecin medecin = (Medecin) medecinRepository.findByName("Taher");

			RendezVous rendezVous = new RendezVous();
			rendezVous.setDateRDV(new Date());
			rendezVous.setStatus(StatusRDV.carcinologique);
			rendezVous.setMedecin(medecin);
			rendezVous.setPatient(patient);
			// rendezVousRepository.save(rendezVous);
			RendezVous savedRDV = servicePack.saveRendezVous(rendezVous);
			System.out.println("SAVED RDV : " + savedRDV.getId() + " " + savedRDV.getStatus());

			// RendezVous rendezVous2 = rendezVousRepository.findById(1L).get();
			RendezVous rendezVous2 = rendezVousRepository.findAll().get(0);
			Consultation consultation = new Consultation();
			consultation.setDateConsultation(new Date());
			consultation.setRapport("Le rapport de la consultation est ....");
			consultation.setRendezVous(rendezVous2);
			// consultationRepository.save(consultation);
			servicesImpl.saveConsultation(consultation);
		};
	}
}