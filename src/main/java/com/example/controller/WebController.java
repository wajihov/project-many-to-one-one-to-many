package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.entities.Patient;
import com.example.repositories.PatientRepository;

@RestController
public class WebController {

	@Autowired
	private PatientRepository patientRepository;

	@GetMapping(path = "/patients")
	public List<Patient> listPatient() {
		return patientRepository.findAll();
	}
}
