package com.example.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.entities.Patient;

public interface PatientRepository extends JpaRepository<Patient, Long> {

	Patient findByNom(String name);
}
