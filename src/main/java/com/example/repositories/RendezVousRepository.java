package com.example.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.entities.RendezVous;

public interface RendezVousRepository extends JpaRepository<RendezVous, String> {

}
