package com.example.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.entities.Consultation;

public interface ConsultationRepository extends JpaRepository<Consultation, Long> {

}
