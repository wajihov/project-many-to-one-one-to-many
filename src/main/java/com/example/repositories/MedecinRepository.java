package com.example.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.entities.Medecin;

public interface MedecinRepository extends JpaRepository<Medecin, Long> {

	Medecin findByName(String nom);

}
