package com.example.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
public class RendezVous {

	@Id
	// @GeneratedValue(strategy = GenerationType.IDENTITY)
	private String id;
	@Temporal(TemporalType.DATE)
	private Date dateRDV;
	@Enumerated(EnumType.STRING)
	private StatusRDV status;
	@ManyToOne
	@JsonProperty(access = Access.WRITE_ONLY)
	private Patient patient;
	@ManyToOne
	private Medecin medecin;
	@OneToOne(mappedBy = "rendezVous")
	private Consultation consultation;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getDateRDV() {
		return dateRDV;
	}

	public void setDateRDV(Date dateRDV) {
		this.dateRDV = dateRDV;
	}

	public StatusRDV getStatus() {
		return status;
	}

	public void setStatus(StatusRDV status) {
		this.status = status;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public Medecin getMedecin() {
		return medecin;
	}

	public void setMedecin(Medecin medecin) {
		this.medecin = medecin;
	}

	public Consultation getConsultation() {
		return consultation;
	}

	public void setConsultation(Consultation consultation) {
		this.consultation = consultation;
	}

	public RendezVous(String id, Date dateRDV, StatusRDV status, Patient patient, Medecin medecin,
			Consultation consultation) {
		super();
		this.id = id;
		this.dateRDV = dateRDV;
		this.status = status;
		this.patient = patient;
		this.medecin = medecin;
		this.consultation = consultation;
	}

	public RendezVous() {
		super();
	}

}